<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
        <fo:root>
            <!-- setup master page and content page:
                 http://w3schools.sinsixx.com/xslfo/obj_layout-master-set.asp.html
                 http://w3schools.sinsixx.com/xslfo/obj_simple-page-master.asp.html
                 http://w3schools.sinsixx.com/xslfo/obj_region-body.asp.html
            -->
            <fo:layout-master-set>
                <fo:simple-page-master master-name="Countries"
                                       page-height="297mm" page-width="210mm" margin="20mm">
                    <fo:region-body/>
                    <fo:region-after extent="10mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
        
            <fo:page-sequence master-reference="Countries">
        
                <!-- add page number on the last 4mm of the page -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-size="4mm" text-align="right">
                        <fo:page-number/>
                    </fo:block>
                </fo:static-content>
        
                <fo:flow flow-name="xsl-region-body">
                    <!-- configure first page of the document with infomation -->
                    <fo:block break-after="page">
                        <fo:block text-align="center" font-size="20mm" margin-top="50mm">
                            <xsl:text>Semestralni</xsl:text>
                        </fo:block>
                        <fo:block text-align="center" font-size="20mm" margin-top="5mm">
                            <xsl:text>Prace</xsl:text>
                        </fo:block>
                        <!-- details of the user -->
                        <fo:block text-align="center" font-size="5mm" margin-top="20mm">
                            <xsl:text>Tomas Klas</xsl:text>
                        </fo:block>
                        <fo:block text-align="center" font-size="5mm" margin-top="5mm">
                            <xsl:text>klastom1</xsl:text>
                        </fo:block>
                        <fo:block text-align="center" font-size="5mm" margin-top="5mm">
                            <xsl:text>klastom1@fit.cvut.cz</xsl:text>
                        </fo:block>
                    </fo:block>

                    <!-- create content of the pdf on next page -->
                    <fo:block break-after="page">
                        <fo:block font-size="7mm" text-align="center">Obsah</fo:block>
                        <!-- run throught country and create content -->
                        <xsl:for-each select="//countries/country">

                            <!-- align to the end -->
                            <fo:block font-size="4mm" text-align-last="justify">
                                <!-- create internal link for the name -->
                                <fo:basic-link internal-destination="{generate-id(.)}">
                                    <xsl:value-of select="@name"/>
                                    <!-- row of a repeating character or cyclically repeating pattern -->
                                    <fo:leader leader-pattern="dots"/>
                                    <!-- The <fo:page-number-citation> object references the page-number 
                                         for the page that contains the first normal area returned by the cited object. -->
                                    <fo:page-number-citation ref-id="{generate-id(.)}"/>
                                </fo:basic-link>
                            </fo:block>
                        
                            <!-- for each section generate link and page number to ref -->
                            <xsl:for-each select="./section">
                                <fo:block margin-left="5mm" text-align-last="justify">
                                    <!-- generate internal link -->
                                    <fo:basic-link internal-destination="{generate-id(.)}">
                                        <xsl:value-of select="@title"/>
                                        <fo:leader leader-pattern="dots"/>
                                        <!-- generate link to that link -->
                                        <fo:page-number-citation ref-id="{generate-id(.)}"/>
                                    </fo:basic-link>
                                </fo:block>
                            </xsl:for-each>
                        
                        </xsl:for-each>
                    </fo:block>
                    <!-- use template for the coutry -->
                    <xsl:apply-templates select="//countries/country"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <!-- template for country refered in countries -->
    <xsl:template match="country">
        <fo:block break-after="page" id="{generate-id(.)}">
            <fo:block font-size="10mm">
                <!-- Select the content of the name -->
                <xsl:value-of select="@name"/>
            </fo:block>
            <fo:block font-size="4mm" margin-bottom="15mm">
                <!-- Select the content of the region -->
                <xsl:value-of select="@region"/>
            </fo:block>
            <fo:block break-after="page" alignment-adjust="baseline">
                <fo:external-graphic  src="/home/tklas/Documents/XMLSemestralka/Countries/{@name}-flag.png"/>
            </fo:block>
            <fo:block break-after="page" alignment-adjust="baseline">
                <fo:external-graphic  src="/home/tklas/Documents/XMLSemestralka/Countries/{@name}-map.png"/>
            </fo:block>
            <xsl:apply-templates select="section"/>
        </fo:block>
    </xsl:template>

    <!-- template for section refered in country -->
    <xsl:template match="section">
        <fo:block id="{generate-id(.)}" margin-bottom="5mm">
            <fo:block font-size="7mm">
                <!-- Select the content of the title -->
                <xsl:value-of select="@title"/>
                <!-- insert a text into the element -->
                    <!-- <xsl:text> - </xsl:text> -->
                <!-- Select the content of the ../@name -->
                <!-- <xsl:value-of select="../@name"/> -->
            </fo:block>
            <!-- refer to the template for category -->
            <xsl:apply-templates select="category"/>
        </fo:block>
    </xsl:template>

    <!-- template for category refered in section -->
    <xsl:template match="category">
        <fo:block margin-left="5mm" margin-bottom="3mm">
            <fo:block font-size="5mm">
                <xsl:value-of select="@title"/>
                <xsl:text>:</xsl:text>
            </fo:block>
            <!-- refer to the template for content -->
            <xsl:apply-templates select="content"/>
        </fo:block>
    </xsl:template>

    <!-- template for content refered in category -->
    <xsl:template match="content">
        <fo:block margin-left="5mm">
            <!-- choose is like if(when), otherwise(else) -->
            <xsl:choose>
                <!-- title is optional so do this block if it is set -->
                <xsl:when test="@title">
                    <fo:block font-size="4mm">
                        <fo:inline font-weight="bold">
                            <xsl:value-of select="@title"/>
                            <xsl:text>:</xsl:text>
                        </fo:inline>
                        <!-- selects the current node as if it would be a folder on the PATH -->
                        <xsl:value-of select="."/>
                    </fo:block>
                </xsl:when>
                <!-- othervise use just a text in the node-->
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>
