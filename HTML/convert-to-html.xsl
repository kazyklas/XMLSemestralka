<xsl:stylesheet version='2.0' 
                xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    <xsl:output indent='yes' method='html' version='5.0'/>

    <!-- start from the countries -->
    <xsl:template match="/">
        <!-- use template for country specified below as a page -->
        <xsl:apply-templates select="countries/country" mode="page"/>
        <xsl:result-document href="HTML/index.html">
            <html>
                <link rel="stylesheet" type="text/css" href="styles.css"/>
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>  
                <head>  
                    <title>Semestralni prace 🌎</title>
                </head>
                <body>
                    <h1>Countries</h1>
                    <ul>
                        <!-- apply templates for country -->
                        <xsl:apply-templates select="countries/country" mode="menu"/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <!-- template for country  -->
    <xsl:template match="country" mode="menu">
        <div class="menu">
            <p><p></p><p></p><p></p>
                <a href="{@name}.html" ><img src="../Countries/{@name}-flag.png"></img></a>
            </p> 
            <h2>
                <a href="{@name}.html">
                    <xsl:value-of select="@name"/>
                </a>
            </h2>
            <h2>
                <a class="button{@name}" href="{@name}.html"/>
            </h2>
            <xsl:apply-templates select="section" mode="menu"/>
        </div>
    </xsl:template>

    <!-- template for section -->
    <xsl:template match="section" mode="menu">
        <!-- add item to the list -->
        <li>
            <!-- add link for the part of the xml into the menu -->
            <a href="{../@name}.html/#{@title}-line">
                <xsl:value-of select="@title"/>
            </a>
        </li>
    </xsl:template>

    <!-- add page for each country -->
    <xsl:template match="country" mode="page">
        <!-- setup the output file for each country -->
        <xsl:result-document href="HTML/{@name}.html">
            <html>
            <link rel="stylesheet" type="text/css" href="styles.css"/> 
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <head>
                    <title>
                        <xsl:value-of select="@name"/>
                    </title>
                </head>
                <body>
                    <h1>
                        <!-- set the heading of the page to value of region and country -->
                        <b><xsl:value-of select="@region"/></b>
                        <xsl:text> - </xsl:text>
                        <xsl:value-of select="@name"/>
                    </h1>
                    <ul>
                        <xsl:apply-templates select="section"/>
                    </ul>
                </body>
            </html>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="section">
           <li id="{@title}" class="anchor">
               <!-- title of the section and the name -->
               <b><a name="{@title}-line"><xsl:value-of select="@title"/></a></b>
           </li>
           <li class="content">
               <xsl:apply-templates select="category"/>
           </li>
    </xsl:template>

    <xsl:template match="category">
        <div class="anchor">
            <xsl:value-of select="@title"/>
        </div>
        <div class="content">
            <xsl:apply-templates select="content"/>
            <br/>
        </div>
    </xsl:template>

    <xsl:template match="content">
        <!-- title is optional so do this block if it is set -->
        <xsl:choose>
            <!-- works as if -->
            <xsl:when test="@title">
                <b><xsl:value-of select="@title"/></b>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="."/>
                <br/>
            </xsl:when>
            <!-- works as else -->
            <xsl:otherwise>
                <!-- selects the current node as if it would be a folder on the PATH -->
                <xsl:value-of select="." disable-output-escaping="yes"/>
                <br/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
