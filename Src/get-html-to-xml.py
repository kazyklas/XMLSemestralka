import bs4, subprocess
import requests
import xml.etree.ElementTree as et
import xml.dom.minidom as minidom
import sys
import glob, re

URLS = ['https://www.cia.gov/library/publications/the-world-factbook/geos/nl.html',
        'https://www.cia.gov/library/publications/the-world-factbook/geos/pl.html',
        'https://www.cia.gov/library/publications/the-world-factbook/geos/sw.html',
        'https://www.cia.gov/library/publications/the-world-factbook/geos/uk.html']

for url in URLS:
    output = "Countries/" + url.split('/')[-1] + ".xml" 
    print(url, " -> ", output)
    
    # get content of the url into the variable
    res = requests.get(url)
    
    # delete all newlines
    html = re.sub('\n', '', res.text)
    
    # run that html throught parser for html
    # http://www.compjour.org/warmups/govt-text-releases/intro-to-bs4-lxml-parsing-wh-press-briefings/
    soup = bs4.BeautifulSoup(html, features="html.parser")
    
    # get expandcollapse from the html (For example Transportation, Communication on the page)
    # can be found in sources of the page
    elems = soup.select('.expandcollapse li')
    country = soup.select('.wfb-text-box .region1')
    
    # https://docs.python.org/2/library/xml.etree.elementtree.html
    # create countryElement that will help us parse and split the html
    countryElement = et.Element('country')
    
    # set name and region key values with country that we get on line 28
    # out: <country name="Netherlands Print" region="Europe">
    countryElement.set('name', country[0].getText().split(':')[-1].strip())
    countryElement.set('region', country[0].getText().split(':')[0].strip())
    
    currentElement = None
    for elem in elems:
        # if the id contains anchor do the following
        if elem.get('id').split('-')[-1] == 'anchor':
            # create element as a subelement and as a section with a specific title
            # out: <section title="Geography">
            currentElement = et.SubElement(countryElement, 'section')
            currentElement.set('title', elem.getText().split(':')[0].strip())
        else:
            element = None
            for sub_elem in elem.select('li > div'):
                if sub_elem.get('id').split('-')[1] == 'anchor':
                    # create category
                    element = et.SubElement(currentElement, 'category')
                    element.set('title', sub_elem.getText().strip()[:-1])
                else:
                    for sub_sub_elem in sub_elem.select('li > div > div'):
                        try:
                            if 'attachment' in sub_sub_elem['class']:
                                continue
                        except:
                            pass
                        # create content and get the text into the tag
                        curr = et.SubElement(element, 'content')
                        if not sub_sub_elem.select('.subfield-name'):
                            text = sub_sub_elem.getText().strip()
                            curr.text = text.strip()
                        else:
                            # get text from spans and add it into the content
                            curr.set('title', sub_sub_elem.select('span')[0].getText().strip()[:-1])
                            text = ""
                            for span in sub_sub_elem.select('span')[1:]:
                                text += span.getText().strip()
                            curr.text = text.strip()

    # decode text to utf-8 and create string that holds it
    string_xml = et.tostring(countryElement).decode("utf-8")
    # parse the string into the XML file and write it to the given file
    out_xml = minidom.parseString(re.sub('\s+', ' ', string_xml)).toprettyxml(indent="    ")
    open(output, 'w').write(out_xml)

# Get all the files with xml content
sources = glob.glob("Countries/*.xml")

# Create the file 
stringOut = "<countries>"

for i in sources:
    # with the help of xml.etree.ElementTree we will parse and read each file
    stringOut += et.tostring(et.parse(i).getroot()).decode("utf-8")

stringOut += "</countries>"

# \s+ = whitespace, so we merge all whitespaces into one space
stringOut = re.sub('\s+', ' ', stringOut)
outXml = minidom.parseString(stringOut).toprettyxml(indent="    ", encoding="UTF-8")
# write to the file with substituted \r\n\f\v as newlines
open("Countries/allCountries.xml", 'w').write(re.sub('[\r\n\f\v]+\s*[\r\n\f\v]+', '\n', outXml.decode("utf-8")))

subprocess.run(['sed', '-i' , 's/ Print//g', 'Countries/allCountries.xml'])
